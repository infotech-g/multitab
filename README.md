# CapyBrowser

CapyBrowser is a project that was originally written for one of the tasks as part of the Code Week competition organized by the INFOTECH Programming Technical School in 2022. The main functionality of the created application is the simultaneous display of many websites in one program window. Everything was written in Python, and more specifically in the popular PyQt5 library.

### Table of contents:

- [Running from sources](#running-from-sources)
- [Classes](#classes)
- [Authors](#authors)

## Running from sources

To get a local copy up and running follow these simple example steps.

Remember that you also need to have installed Python on your computer (application is compatible with version 3.9).

### Installation

1. Clone the repo
  ```sh
  git clone https://gitlab.com/infotech-g/capybrowser.git
  ```
2. Navigate To Directory
  ```sh
  cd capybrowser
  ```
3. Install pip packages
  ```sh
  pip install -r requirements.txt
  ```
4. Run the Program
  ```sh
  python3 src/main.py
  ```
Note: If the last command does not work, try
```sh
python src/main.py
```

## Classes

We can distinguish the following classes in the application:

- [App](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/app.md) - main window of the application
- [BookmarksBar](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/bookmarks-bar.md) - bar that contains saved bookmarks
- [Display](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/display.md) -  widget that contains NavBar, BookmarksBar and the browser
- [NavBar](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/nav-bar.md) - bar that contains search bar and buttons that allows to save bookmark and open new display 
- [Page](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/page.md) - widget that can contain multiple Displays
- [TabsWidget](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/tab.md) - widget that allows to switch between Pages
- [RedirectWrapper](https://gitlab.com/infotech-g/capybrowser/-/blob/main/docs/wrapper.md) - connects bookmark button onClick method with redirection function

## Authors

- [@adam_czarkowski](https://gitlab.com/adam.czarkowski)
- [@olgierd_kornacki](https://gitlab.com/olgierd.kornacki)
- [@norbert_szorc](https://gitlab.com/norbert.szorc)
