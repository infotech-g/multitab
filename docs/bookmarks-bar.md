# Class: BookmarksBar

Class defining a bar that contains saved bookmarks.

## Parent class

- **QWidget** from PyQt5.QtWidgets

## Props

- **parent** - variable that declares the parent object

## Methods

- **add_bookmark**
    - Arguments
        - short_url
        - from_file 
    - Functionalities
        - adds new bookmark
    - Returns
        - this method doesn't return anything
- **remove_bookmark**
    - Arguments
        - this method doesn't have any arguments
    - Functionalities
        - removes last saved bookmark
    - Returns
        - this method doesn't return anything

