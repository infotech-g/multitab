# Class: RedirectWrapper

Class that connects bookmark button onClick method with redirection function.

## Parent class

No parent class available

## Props

- **url** - variable that declares domain's url to redirect
- **display** - variable that declares which display redirect function will target 

## Methods

- **redirect**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - connects bookmark button onClick method with redirection function 
    - Returns
        - this method doesn't return anything


