# Class: Page

Class defining a widget that can contain multiple Displays.

## Parent class

- **QWidget** from PyQt5.QtWidgets

## Props

- **parent** - variable that declares the parent object

## Methods

- **add_display**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - adds new Display object to layout
    - Returns
        - this method doesn't return anything
