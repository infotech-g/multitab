# File: setting.py

File defining const variables.

## Variables

- **APP_TITLE** - variable that declares the title of the application
- **DEFAULT_BROWSER_URL** - variable that declares an url that Display class uses as default on object init
- **DEFAULT_BROWSER_SHORT_URL** - variable that declares an url that url bar from NavBar class displays as default on object init
- **ASSETS_PATH** - variable that declares path to assets files


