# Class: NavBar

Class defining a bar that contains search bar and buttons that allows to save bookmark and open new display.

## Parent class

- **QWidget** from PyQt5.QtWidgets

## Props

- **parent** - variable that declares the parent object

## Methods

- **search**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - runs url from url bar in related Display object
    - Returns
        - this method doesn't return anything
- **add_to_bookmarks**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - runs add_bookmark method from BookmarksBar class
        - saves new bookmark to save_data.txt file
    - Returns
        - this method doesn't return anything
- **remove_from_bookmarks**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - runs remove_bookmark method from BookmarksBar class
    - Returns
        - this method doesn't return anything

