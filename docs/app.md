# Class: App

Class defining the main window of the application.

## Parent class

- **QMainWindow** from PyQt5.QtWidgets

## Props

No props available

## Methods

No functions available
