# Class: TabsWidget

Class defining a widget that allows to switch between Pages.

## Parent class

- **QWidget** from PyQt5.QtWidgets

## Props

- **parent** - variable that declares the parent object

## Methods

- **add_new_tab**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - adds new tab with Page object to layout 
    - Returns
        - this method doesn't return anything
- **remove_last_page**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - removes last Page with related tab to it 
    - Returns
        - this method doesn't return anything
