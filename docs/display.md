# Class: Display

Class defining a widget that contains NavBar, BookmarksBar and the browser.

## Parent class

- **QWidget** from PyQt5.QtWidgets

## Props

- **parent** - variable that declares the parent object

## Methods

- **_set_layout**
    - Arguments
        - this method doesn't have any arguments 
    - Functionalities
        - sets widget layout/structure
    - Returns
        - this method doesn't return anything

