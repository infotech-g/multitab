from PyQt5.QtCore import QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QVBoxLayout, QWidget

from bookmarks_bar import BookmarksBar
from nav_bar import NavBar
from settings import DEFAULT_BROWSER_URL


class Display(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.layout = QVBoxLayout(self)
        self.navbar = NavBar(self)
        self.browser = QWebEngineView(self)
        self.browser.setUrl(QUrl(DEFAULT_BROWSER_URL))
        self.bookmarks = BookmarksBar(self)
        self._set_layout()

    def _set_layout(self):
        self.layout.addWidget(self.navbar)
        self.layout.addWidget(self.bookmarks)
        self.layout.addWidget(self.browser)
        self.setLayout(self.layout)
