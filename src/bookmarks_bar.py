import os

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout, QPushButton, QWidget

from wrapper import RedirectWrapper


class BookmarksBar(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.setMaximumHeight(30)
        self.layout = QGridLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.number_of_buttons = 0
        self.wrappers = []

        self.setLayout(self.layout)

        try:
            for short_url in open("locale/save_data.txt", "r"):
                self.add_bookmark(short_url, from_file=True)
        except FileNotFoundError:
            os.mkdir("locale")
            open("locale/save_data.txt", "w+")

    def add_bookmark(self, short_url, from_file=False):
        new_bookmark = QPushButton(self)
        new_bookmark.setText(short_url)
        new_bookmark.setMaximumWidth(110)
        new_bookmark.setMaximumHeight(21)

        if from_file:
            redirect = RedirectWrapper(short_url, self.parent)
        else:
            redirect = RedirectWrapper(self.parent.navbar.url_bar.text(), self.parent)

        self.wrappers.append(
            redirect
        )  # We need instances of wrappers to live through program main loop
        new_bookmark.clicked.connect(redirect.redirect)
        self.layout.addWidget(
            new_bookmark, 0, self.number_of_buttons, alignment=Qt.AlignLeft
        )
        self.layout.setColumnStretch(self.number_of_buttons, 0)
        self.layout.setColumnStretch(self.number_of_buttons + 1, 1)
        self.number_of_buttons += 1

    def remove_bookmark(self):
        if self.number_of_buttons <= 0:
            return
        self.layout.removeWidget(
            self.layout.itemAtPosition(0, self.number_of_buttons - 1).widget()
        )
        self._delete_last_line_in_file("locale/save_data.txt")
        self.number_of_buttons -= 1

    def _delete_last_line_in_file(self, path_to_file):
        fd = open(path_to_file, "r")
        d = fd.read()
        fd.close()
        m = d.split("\n")
        s = "\n".join(m[:-1])
        fd = open(path_to_file, "w+")
        for i in range(len(s)):
            fd.write(s[i])
        fd.close()
