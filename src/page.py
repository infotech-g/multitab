from PyQt5.QtWidgets import QHBoxLayout, QSizePolicy, QWidget

from display import Display


class Page(QWidget):
    def __init__(self, parent):
        self.parent = parent
        super().__init__(parent)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.display = Display(self)
        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.display)
        self.setLayout(self.layout)

    def add_display(self):
        self.display = Display(self)
        self.display.setMinimumWidth(600)
        self.layout.addWidget(self.display)
