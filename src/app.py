from PyQt5 import QtGui
from PyQt5.QtWidgets import QMainWindow

from settings import APP_TITLE, ASSETS_PATH
from tab import TabsWidget


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle(APP_TITLE)
        self.setWindowIcon(QtGui.QIcon(f"{ASSETS_PATH}/logo.png"))
        self.tab_widget = TabsWidget(self)
        self.setCentralWidget(self.tab_widget)
        self.showMaximized()
