from PyQt5.QtCore import QUrl


class RedirectWrapper:
    def __init__(self, url, display):
        self.url = url.strip()
        self.display = display

    def redirect(self):
        self.display.browser.setUrl(QUrl(f"https://{self.url}"))
        self.display.navbar.url_bar.setText(self.url)
        pages = self.display.parent.parent.pages
        pages.setTabText(pages.currentIndex(), self.url)
