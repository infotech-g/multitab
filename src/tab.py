from PyQt5.QtWidgets import (QHBoxLayout, QPushButton, QSizePolicy, QTabWidget,
                             QVBoxLayout, QWidget)

from page import Page
from settings import DEFAULT_BROWSER_SHORT_URL


class TabsWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        layout = QVBoxLayout(self)
        self.pages = QTabWidget(self)
        first_page = Page(self)
        self.pages.addTab(first_page, DEFAULT_BROWSER_SHORT_URL)

        layout.addWidget(self.pages)

        container = QWidget(self)
        container_layout = QHBoxLayout(self)
        container.setGeometry(100, 100, 100, 100)
        add_new_page_button = QPushButton("+", container)
        add_new_page_button.clicked.connect(self.add_new_tab)

        remove_last_page_button = QPushButton("-", container)
        remove_last_page_button.clicked.connect(self.remove_last_page)

        container_layout.addWidget(remove_last_page_button)
        container_layout.addWidget(add_new_page_button)
        container_layout.setContentsMargins(0, 0, 0, 0)
        container.setLayout(container_layout)
        self.pages.setCornerWidget(container)

        self.setLayout(layout)

    def add_new_tab(self):
        new_page = Page(self)
        self.pages.addTab(new_page, "Nowa strona")
        self.pages.setCurrentWidget(new_page)

    def remove_last_page(self):
        if self.pages.count() > 1:
            self.pages.removeTab(self.pages.currentIndex())
