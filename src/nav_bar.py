from PyQt5.QtCore import QUrl
from PyQt5.QtWidgets import QHBoxLayout, QLineEdit, QPushButton, QWidget

from settings import DEFAULT_BROWSER_SHORT_URL


class NavBar(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.setMaximumHeight(20)
        layout = QHBoxLayout(self)
        self.url_bar = QLineEdit(self)
        self.url_bar.setText(DEFAULT_BROWSER_SHORT_URL)
        self.url_bar.returnPressed.connect(self.search)
        layout.addWidget(self.url_bar)
        search_button = QPushButton(self)
        search_button.setText("Szukaj")
        search_button.clicked.connect(self.search)
        layout.addWidget(search_button)
        add_another_display_button = QPushButton(self)
        add_another_display_button.setText("Dodaj ekran")
        add_another_display_button.clicked.connect(self.parent.parent.add_display)
        layout.addWidget(add_another_display_button)
        add_to_bookmarks_button = QPushButton(self)
        add_to_bookmarks_button.setText("Dodaj do zakładek")
        remove_from_bookmarks_button = QPushButton(self)
        remove_from_bookmarks_button.setText("Usuń zakładke")
        remove_from_bookmarks_button.clicked.connect(self.remove_from_bookmarks)
        add_to_bookmarks_button.clicked.connect(self.add_to_bookmarks)
        layout.addWidget(remove_from_bookmarks_button)
        layout.addWidget(add_to_bookmarks_button)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

    def search(self):
        self.parent.browser.setUrl(QUrl(f"https://{self.url_bar.text()}"))
        pages = self.parent.parent.parent.pages
        pages.setTabText(pages.currentIndex(), self.url_bar.text())

    def add_to_bookmarks(self):
        self.parent.bookmarks.add_bookmark(self.url_bar.text())
        save_data_file = open("locale/save_data.txt", "a")
        save_data_file.write(self.parent.navbar.url_bar.text() + "\n")

    def remove_from_bookmarks(self):
        self.parent.bookmarks.remove_bookmark()
